/*
 * AI.cpp
 *
 *  Created on: Mar 31, 2013
 *      Author: Billy
 */

#include "AI.h"
#include "Object.h"
#include "stdlib.h"
#include "GameParameters.h"
#include <ctime>

AI::AI() {
	this->state = AI_SELECT;
	this->totalSteps = 100;
	this->step = 0;
	this->last_time = 0;
	this->last_render_hit_time = 0;
	this->health = 100;
	this->is_messaging = false;
}

AI::~AI() {
}

Vec3& AI::get_pos() {
	return this->drone->pos;
}

Vec3 AI::select_pos() {
	this->step = 0;
	int choose_x = GameParameters::area_limit
			- rand() % (int) GameParameters::area_limit * 2;
	int choose_z = GameParameters::area_limit
			- rand() % (int) GameParameters::area_limit * 2;
	this->go_to_pos = Vec3(choose_x, 0, choose_z);

	Vec3& current_pos = this->get_pos();
	this->diffX = (choose_x - current_pos.x) / totalSteps;
	this->diffZ = (choose_z - current_pos.z) / totalSteps;

	//printf("AI Select %d %d", choose_x, choose_z);
}

void AI::go_to_random_pos() {
	if (this->step >= this->totalSteps) {
		this->step = 0;
		this->state = AI_SELECT;
		return;
	}

	this->drone->pos[0] += diffX;
	this->drone->pos[2] += diffZ;

	if (this->is_close(true)) {
		GameParameters::is_game_over = true;
		GameParameters::is_ai_win = true;
	}

	this->step++;
}

void AI::evade_enemy() {
}

void AI::kill_enemy() {
}

void AI::set_enemy(Object* enemy) {
	this->enemy = enemy;
}

Drone* AI::get_drone() {
	return this->drone;
}

void AI::hit() {
	this->health -= 5;
	if (this->health <= 0) {
		GameParameters::is_game_over = true;
	}
	this->drone->is_render_hit = true;
	this->last_render_hit_time = clock();
}

bool AI::has_message() {
	return this->is_messaging;
}

bool AI::is_close(bool is_very) {
	if (enemy == NULL) {
		return false;
	}
	Vec3& pos = this->get_pos();
	Vec3& player_pos = enemy->pos;

	int tol =
			(is_very) ?
					GameParameters::AI_DEATH_TOLERANCE :
					GameParameters::AI_MSG_TOLERANCE;

	return abs(player_pos.length() - pos.length()) < tol;
}

const char* AI::get_message() {
	this->is_messaging = false;
	switch (rand() % 10) {
	case 0:
		return "Ai: don't kill mee!11\0";
		break;
	case 1:
		return "Ai: pREss the red X buttn at the top right to shoot me\0";
		break;
	case 2:
		return "Ai: Don't do it\0";
		break;
	case 3:
		return "Ai: Hello!\0";
		break;
	case 4:
		return "Ai: Prepare to die!\0";
		break;
	case 5:
		return "Ai: This is going to be easy...\0";
		break;
	case 6:
		return "Ai: ...\0";
		break;
	case 7:
		return "Ai: Beat it, punk\0";
		break;
	case 9:
		return "Ai: You're a loser!\0";
		break;
	}
	return "\0";
}

void AI::make_decision() {
	if (GameParameters::is_game_over) {
		return;
	}

	int decision = rand() % 2;
	if (decision == 0) {
		select_pos();
		this->state = AI_GOTO;

		if (this->is_close(false)) {
			this->is_messaging = true;
		} else {
			this->is_messaging = false;
		}
	} else {
		//Don't message the user yet
		this->is_messaging = false;

		this->state = AI_STANDBY;
		this->standby_steps = rand() % 40;
		standby();
	}

}

void AI::update(float dt) {
	/*printf("time diff %ld\n", t - last_time);
	 fflush(stdout);*/
	//if (dt - last_render_hit_time > 1000)
	if (dt > 10) {
		this->drone->is_render_hit = false;
	}
	if (dt > 5)
	//if (dt - last_time > 150)
			{
		switch (this->state) {
		case AI_SELECT:
			make_decision();
			break;
		case AI_GOTO:
			go_to_random_pos();
			break;
		case AI_EVADE:
			break;
		case AI_KILL:
			break;
		case AI_STANDBY:
			standby();
			break;
		}
		last_time = dt;
	}
	//needs to be rendered again
	this->render();
}

void AI::set_representative(Drone* drone) {
	this->drone = drone;
}

void AI::render() {
	this->drone->draw();
}

Vec3 AI::get_enemy_pos() {
	return this->enemy->pos;
}

void AI::standby() {
	this->step++;
	if (this->step >= this->standby_steps) {
		this->step = 0;
		this->state = AI_SELECT;
	}
}
