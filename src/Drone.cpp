/*
 * Drone.cpp
 *
 *  Created on: Mar 31, 2013
 *      Author: Edward
 */

#include "Drone.h"

#if defined(__APPLE_CC__)
#include<OpenGL/gl.h>
#include<OpenGL/glu.h>
#include<GLUT/glut.h>
#elif defined(WIN32)
#include<windows.h>
#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glut.h>
#else
#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glut.h>
#include<stdint.h>
#endif

#include "SphereProxy.h"

Drone::Drone(Vec3 _pos, Vec3 _vel, Vec3 _acc) :
		Character(_pos, _vel, _acc, 3, 50, 100) {
	proxy = new SphereProxy(this, DRONE_RADIUS);
	is_render_hit = false;
}

Drone::~Drone() {
	delete proxy;
}

Vec3 Drone::get_proxy_pos() {
	return pos;
}

void Drone::draw() {
	glPushMatrix();
	glTranslatef(0, DRONE_HOVER_HEIGHT, 0);

	if (is_render_hit) {
		glColor3f(1, 0, 0);
	} else {
		glColor3f(1, 1, 1);
	}
	//Torso
	glutSolidCube(3);

	glPushMatrix();
	//Draw head
	glTranslatef(0, 1.5, 0);
	if (!is_render_hit) {
		glColor3f(0, 0, 1);
	}
	glutSolidSphere(1.3, 20, 20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(2, -.3, 0);
	draw_arm();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-2, -.3, 0);
	draw_arm();
	glPopMatrix();

	glPopMatrix();
}

void Drone::draw_arm() {
	glColor3f(.5, .5, .5);
	glScalef(.5, 2, .5);
	glutSolidCube(2);
}
