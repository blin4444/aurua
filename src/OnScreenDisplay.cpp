/*
 * OnScreenDisplay.cpp
 *
 *      Author: Billy
 */
#include <ctime>
#if defined(__APPLE_CC__)
#include<OpenGL/gl.h>
#include<OpenGL/glu.h>
#include<GLUT/glut.h>
#elif defined(WIN32)
#include<windows.h>
#else
#include<stdint.h>
#endif
#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glut.h>
#include "OnScreenDisplay.h"
#include "GameParameters.h"
#include "main.h"

namespace OnScreenDisplay
{
bool is_crosshair_ai = false;
const char* last_message;

time_t last_message_time = 0;
char last_fps[8];

void print_life()
{
	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, 0, height);
	glScalef(1, 1, 1);
	glTranslatef(0, 0, 1);
	glMatrixMode (GL_MODELVIEW);

	if (GameParameters::is_game_over)
	{
		glRasterPos2f(width / 2 - 80, height / 2);
		if (GameParameters::is_ai_win)
		{
			print_bitmap_string(GLUT_BITMAP_HELVETICA_18,
					"Game over! You lose!");
		}
		else
		{
			print_bitmap_string(GLUT_BITMAP_HELVETICA_18,
					"Game over! You win!");
		}
		return;
	}

	char string[64];
	/*snprintf(string, 32, "%d %f %f", 100, game.player->pos.y,
	 Terrain::get_height(game.player->pos.x, game.player->pos.z));*/

	snprintf(string, 64, "Health: %d \n AI Health: %d", game.player->stamina,
			game.ai.getHealth());
	glColor3f(1, 1, 1);
	glDisable (GL_TEXTURE_2D);
	double tmp[4];
	GLboolean valid;
	glGetDoublev(GL_CURRENT_RASTER_POSITION, tmp);
	glGetBooleanv(GL_CURRENT_RASTER_POSITION_VALID, &valid);
	//print_vector(tmp);
	//printf("%s\n", valid ? "valid" : "INVALID");
	glRasterPos2f(20, 20);

	print_bitmap_string(GLUT_BITMAP_HELVETICA_18, string);

	time_t curr_time = time(NULL);
	bool is_consider_message = curr_time - last_message_time < 10;
	bool has_message = game.ai.has_message();
	if (has_message || is_consider_message)
	{
		last_message_time = curr_time;

		char insult[64];
		if (has_message)
		{
			const char* msg = game.ai.get_message();
			snprintf(insult, 64, msg);
			last_message = msg;
		}
		else
		{
			snprintf(insult, 64, last_message);
		}

		glRasterPos2f(20, 60);

		print_bitmap_string(GLUT_BITMAP_HELVETICA_18, insult);
	}

	if (GameParameters::is_show_fps)
	{
		glRasterPos2f(width - 40, height - 40);
		set_fps(last_fps);
		print_bitmap_string(GLUT_BITMAP_HELVETICA_18, last_fps);
	}

	//print_stroke_string(GLUT_STROKE_ROMAN, string);
}

// Draw cross hair, print life and stamina amount
void draw_HUD()
{
	glDisable (GL_LIGHTING);

	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);
	float h_w_ratio = float(height) / width;

	glDisable (GL_DEPTH_TEST);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -1.0, 1.0, -10.0, 10.0);

	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();

	if (!GameParameters::is_game_over)
	{
		draw_crosshair(is_crosshair_ai, h_w_ratio);
	}
	print_life();
}

/**
 * is_on specifies whether or not this is on target
 */
void draw_crosshair(bool is_on, float h_w_ratio) {
	if (is_on)
	{
		glColor4f(1, 0, 0, 1);
		glLineWidth(8);
	}
	else
	{
		glColor4f(0.0, 1.0, 0, 0.8);
		glLineWidth(3);
	}

	glBegin (GL_LINES);
	glVertex3f(0, -0.05, 0);
	glVertex3f(0, -0.01, 0);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(0, 0.01, 0);
	glVertex3f(0, 0.05, 0);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(-0.05 * h_w_ratio, 0, 0);
	glVertex3f(-0.01 * h_w_ratio, 0, 0);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(0.01 * h_w_ratio, 0, 0);
	glVertex3f(0.05 * h_w_ratio, 0, 0);
	glEnd();

	glLineWidth(1);
}

// s is a 0-terminated string
void print_bitmap_string(void* font, char* s) {
	if (s && strlen(s)) {
		while (*s) {
			glutBitmapCharacter(font, *s);
			s++;
		}
	}
}
void print_stroke_string(void* font, char* s) {
	if (s && strlen(s)) {
		while (*s) {
			glutStrokeCharacter(font, *s);
			s++;
		}
	}
}

// increment frame_passed every call, update FPS value every one second
void set_fps(char* buf) {
	unsigned long curr_time = Utils::get_system_time();
	int current_window = glutGetWindow();
	if (current_window == cam_window) {
		frame_passed++;
		if (curr_time - prev_fps_time > 1000.0) {
			sprintf(buf, "%d fps", (int) frame_passed);
			//glutSetWindowTitle(win_title);
			frame_passed = 0;
			prev_fps_time = curr_time;
		}
	}

}
}
