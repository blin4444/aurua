/*
 * ParticleEngine.cpp
 *
 *  Created on: Apr 7, 2013
 *      Author: Edward
 */
#include <algorithm>
#include "ParticleEngine.h"

ParticleEngine::ParticleEngine(Player* _player) {
	player = _player;
}

ParticleEngine::~ParticleEngine() {
}

void ParticleEngine::step(float dt_ms) {

	for (vector<Particle*>::iterator itr = particles.begin();
			itr != particles.end();) {
		Particle* p = *itr;

		p->pos += p->vel * dt_ms;
		p->time_alive += dt_ms;

		if (p->time_alive > p->lifespan) {
			itr = particles.erase(itr);
			delete p;
		} else {
			itr++;
		}
	}
}

bool ParticleEngine::operator()(Particle* p1, Particle* p2) {
	Vec3 cam_pos = player->get_eye();
	return (p1->pos - cam_pos).length() > (p2->pos - cam_pos).length();
}

void ParticleEngine::draw() {
	Vec3 tmp;
	Vec3 cam_pos = player->get_eye();
	Vec3 up = player->cam->up;
	vector<Particle*> sorted_p(particles);
	sort(sorted_p.begin(), sorted_p.end(), *this);
	float size = PARTICLE_SIZE / 2;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Textures::textures[Textures::PARTICLE]);
	glBegin(GL_QUADS);
	for (vector<Particle*>::iterator itr = sorted_p.begin();
			itr != sorted_p.end(); itr++) {
		Particle* p = *itr;
		Vec3 view = p->pos - cam_pos;
		Vec3 u = view.cross(up);
		u.normalize();
		Vec3 v = u.cross(view);
		v.normalize();
		glColor4f(p->color[0], p->color[1], p->color[2],
				(1 - p->time_alive / p->lifespan));
		tmp = p->pos - u * size - v * size;
		glTexCoord2f(0, 0);
		glVertex3f(tmp.x, tmp.y, tmp.z);
		glTexCoord2f(0, 1);
		tmp = p->pos - u * size + v * size;
		glVertex3f(tmp.x, tmp.y, tmp.z);
		glTexCoord2f(1, 1);
		tmp = p->pos + u * size + v * size;
		glVertex3f(tmp.x, tmp.y, tmp.z);
		glTexCoord2f(1, 0);
		tmp = p->pos + u * size - v * size;
		glVertex3f(tmp.x, tmp.y, tmp.z);
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

// returns random float between -1 and 1
float ParticleEngine::rand_float() {
	return ((float) rand() - RAND_MAX / 2) / (RAND_MAX / 2);
}

Vec3 ParticleEngine::rand_vec3() {
	Vec3 vec = Vec3(rand_float(), rand_float(), rand_float());
	vec.normalize();
	vec *= (((float) rand() / (float) RAND_MAX) / 100);
	return vec;
}

void ParticleEngine::add(Particle* p) {
	particles.push_back(p);
}
