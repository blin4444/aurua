#if defined(__APPLE_CC__)
#include<OpenGL/gl.h>
#include<OpenGL/glu.h>
#include<GLUT/glut.h>
#elif defined(WIN32)
#include<windows.h>
#else
#include<stdint.h>
#endif
#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glut.h>

#include<iostream>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<string.h>

#include "main.h"
#include "ViewingMode.h"
#include "Textures.h"
#include "Camera.h"
#include "Vec3.h"
#include "Utils.h"
#include "Geometry.h"
#include "PhysicsEngine.h"
#include "Player.h"
#include "Terrain.h"
#include "Drone.h"
#include "Bolt.h"
#include "GameParameters.h"
#include <ctime>
#include "OnScreenDisplay.h"
#include "ParticleEngine.h"

using namespace std;
using namespace Textures;

GLfloat position0[] = { 10.0, 20.0, 10.0, 0.0 };
GLfloat position1[] = { -10.0, 20.0, -10.0, 0.0 };

int sky_stacks = 16;

Game game;

bool sky_init = false;

// select buffer size
#define BUFSIZE 512

// flag to indicate that we should clean up and exit
bool is_quit = false;
bool paused = false;

// Keys variables
bool key_states[256];
bool special_states[256];
bool mouse_states[3];

// window handles
int main_window, cam_window;
unsigned long prev_fps_time;
unsigned long prev_phys_time;
int frame_passed = 0;
char win_title[32] = { 0 };

// display width and height
int disp_width = 800;
int disp_height = 640;

// Cursor previous position
int prev_x, prev_y;

//Cool debug tool! Use command line arguments to pass in ints!
bool is_use_in_values = false;
double in_value[3];
double cam_rot_speed = 1.0;

extern Game game;
unsigned long last_laser_discharge = 0;
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
/// Initialization/Setup and Teardown ////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// set up opengl state, allocate objects, etc.  This gets called
// ONCE PER WINDOW, so don't allocate your objects twice!
void gl_init() {
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH ), glutGet(GLUT_WINDOW_HEIGHT ));
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);

	// lighting stuff
	GLfloat ambient[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat specular[] = { 0.8, 0.8, 0.8, 1.0 };

	glLightfv(GL_LIGHT0, GL_POSITION, position0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

	GLfloat ambient2[] = { 0.1, 0.1, 0.1, 1.0 };
	GLfloat diffuse2[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat specular2[] = { 0.5, 0.5, 0.5, 1.0 };

	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient2);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specular2);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_COLOR_MATERIAL);
}
void game_init() {
	// game physics
	game.phys = new PhysicsEngine(Vec3(-1000, -1000, -1000),
			Vec3(1000, 1000, 1000));

	// Player initializations
	game.player = new Player(Vec3(5, 5, 5), Vec3(0, 0, 0), Vec3(0, 0, 0), 10);
	game.phys->add(game.player);
	game.current_object = game.player;

	// Other objects
	Drone* drone = new Drone(Vec3(-5, 100, -5), Vec3(0, 0, 0), Vec3(0, 0, 0));
	game.test_drone = drone;
	game.phys->add(drone);
	drone = new Drone(Vec3(-15, 5, -15), Vec3(0, 0, 0), Vec3(0, 0, 0));
	game.ai.set_representative(drone);
	game.phys->add(drone);
	drone = new Drone(Vec3(-50, 0, -50), Vec3(0, 0, 0), Vec3(0, 0, 0));
	game.phys->add(drone);
	drone = new Drone(Vec3(-50, 0, 50), Vec3(0, 0, 0), Vec3(0, 0, 0));
	game.phys->add(drone);
	drone = new Drone(Vec3(50, 0, 50), Vec3(0, 0, 0), Vec3(0, 0, 0));
	game.phys->add(drone);
	drone = new Drone(Vec3(50, 0, -50), Vec3(0, 0, 0), Vec3(0, 0, 0));
	game.phys->add(drone);

// Terrains
	Terrain::init_terrain();

	game.particle = new ParticleEngine(game.player);
	init_pick();
}

void init_pick() {
	glInitNames();
	glPushName(0);
}

// free any allocated objects and return
void cleanup() {
	/////////////////////////////////////////////////////////////
	/// TODO: Put your teardown code here! //////////////////////
	/////////////////////////////////////////////////////////////

	set<Object*>::iterator itr;
	for (itr = game.phys->objects.begin(); itr != game.phys->objects.end();
			itr++) {
		Object* obj = *itr;
		delete obj;
	}
	delete game.phys;
	delete game.particle;
}

const int DIMENSIONS = 3;

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
/// Callback Stubs ///////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// window resize callback
void resize_callback(int width, int height) {
	/////////////////////////////////////////////////////////////
	/// TODO: Put your resize code here! ////////////////////////
	/////////////////////////////////////////////////////////////
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH ), glutGet(GLUT_WINDOW_HEIGHT ));
	glutPostRedisplay();
}

/*void resetCurrentMatrix() {
 for (int x = 0; x < 16; x++) {
 currentMatrix[x] = 0;
 }
 }*/

// keyboard callback
void keyboard_callback(unsigned char key, int x, int y) {
	switch (key) {
	case 27:
		is_quit = true;
		return;
	default:
		break;
	}
}

void special_key_callback(int key, int x, int y) {
	special_states[key] = !special_states[key];
}

void cam_keyboard_callback(unsigned char key, int x, int y) {
	int current_window = glutGetWindow();
	key_states[key] = true;
	if (key == 27) {
		exit(0);
	}
	if (current_window == cam_window) {
		switch (key) {
		case 'p':
		case 'P':
			paused = !paused;
			if (paused) {
				sprintf(win_title, "Paused");
				glutSetWindowTitle(win_title);
				glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
				glutPassiveMotionFunc(NULL);
			} else {
				prev_phys_time = Utils::get_system_time();
				glutTimerFunc(TIMER_MS, idle, 0);
				glutPassiveMotionFunc(motion_callback);
			}
			break;
		case 'W':
			key_states['w'] = true;
			break;
		case 'A':
			key_states['a'] = true;
			break;
		case 'S':
			key_states['s'] = true;
			break;
		case 'D':
			key_states['d'] = true;
			break;
		case 'h':
		case 'H':
			key_states['h'] = true;
			break;
		case 'j':
		case 'J':
			game.current_object = game.test_drone;
			break;
		case 'k':
		case 'K':
			game.current_object = game.player;
			break;
		case 'l':
		case 'L':
			GameParameters::is_show_fps = !GameParameters::is_show_fps;
			break;
		case '_':
			GameParameters::is_game_over = true;
			break;
		case 'r':
		case 'R':
			game.player->pos = Vec3(5, 5, 5);
			game.player->vel = Vec3(0, 0, 0);
			game.player->acc = Vec3(0, 0, 0);
			break;
		case 0x20:
			player_jump();
			break;
		default:
			break;
		}

	}
}

void cam_keyup_callback(unsigned char key, int x, int y) {
	int current_window = glutGetWindow();
	key_states[key] = false;

	if (current_window == cam_window) {
		switch (key) {
		case 'W':
			key_states['w'] = false;
			break;
		case 'A':
			key_states['a'] = false;
			break;
		case 'S':
			key_states['s'] = false;
			break;
		case 'D':
			key_states['d'] = false;
			break;
		default:
			break;
		}

	}
}

void player_jump() {
	if (game.phys->object_on_ground(game.player)) {
		game.player->pos += Vec3(0, 3, 0);
		game.player->acc += Vec3(0, 1000, 0);
	}
}

void object_move(Object* object, Vec3 mov_dir) {
	double max_speed = WALK_SPEED;
	// No movement input, "brake" to stop sliding
	if (mov_dir == Vec3(0, 0, 0)) {
		Vec3 decel = Vec3(object->vel.x, 0, object->vel.z) * -SLOW;
		object->acc += decel;
		return;
	}

#if _WIN32
	// return states of the left shift key
	if (GetKeyState(VK_LSHIFT) & 0x80) {
		max_speed = RUN_SPEED;
	}
#endif
	double curr_speed = object->vel.length();
	// prevent releasing shift to generate a negative scalar
	if (curr_speed > max_speed) {
		curr_speed = max_speed;
	}
	// accelerate slowly if acc and view align
	double dir_vel_len = mov_dir.length() * object->vel.length();
	if (dir_vel_len < 1) {
		dir_vel_len = 1;
	}
	double acc_mod = 4 - (mov_dir.dot(object->vel) / dir_vel_len + 2);
	object->acc += mov_dir * (1 - curr_speed / max_speed) * acc_mod * ACC;
	//debug_player(game.player);
}

void keys_consumer() {
	int current_window = glutGetWindow();

	if (current_window == cam_window) {

		// Camera view relative vectors
		Vec3 mov_f, mov_r, mov_dir;
		Camera* cam = game.player->cam;

		mov_dir = Vec3(0, 0, 0);
		if (!special_states[GLUT_KEY_F1]) {
			// walk mode
			mov_f = Vec3(cam->view.x, 0, cam->view.z);
		} else {
			// fly mode
			mov_f = cam->view;
		}
		mov_f.normalize();
		// for side strafe
		mov_r = mov_f.cross(cam->up);
		mov_r.normalize();

		if (key_states['w']) {
			mov_dir += mov_f;
		} else if (key_states['s']) {
			mov_dir -= mov_f;
		}
		if (key_states['d']) {
			mov_dir += mov_r;
		} else if (key_states['a']) {
			mov_dir -= mov_r;
		}

		if (key_states['h']) {
			Terrain::is_request_print = true;
			Vec3 v = Terrain::get_normal(game.player->pos.x,
					game.player->pos.z);
			//debug_vec3(v);
		}
		if (mov_dir != Vec3(0, 0, 0)) {
			mov_dir.normalize();
		}
		if (game.phys->object_on_ground(game.player)) {
			object_move(game.current_object, mov_dir);
		}
	}
}

void draw_laser() {
	Vec3 cam_f, cam_r, cam_u, aim_dir, lsr_start, lsr_end, lsr_seg, lsr_dir,
			seg_start, seg_end;
	Camera* cam = game.player->cam;
	cam_f = Vec3(cam->view.x, 0, cam->view.z);
	cam_r = cam_f.cross(cam->up);
	cam_u = cam_r.cross(cam_f);
	cam_r.normalize();
	lsr_start = game.player->get_eye() + cam_r - cam_u;
	lsr_end = game.player->get_eye() + cam->view * 50;
	lsr_seg = (lsr_end - lsr_start) / laser_segs;
	lsr_dir = lsr_seg;
	lsr_dir.normalize();

	int i;
	float rand_u, rand_r;
	// Particles simulate carbons
	for (i = 0; i < 5; i++) {
		Particle* p = (Particle*) malloc(sizeof(Particle));
		p->color = Vec3(0, 0, 0);
		p->pos = lsr_start + lsr_dir * (rand() % 500) + lsr_dir * 10;
		rand_u = laser_segs_max_dev
				* (((float) rand() - RAND_MAX / 2) / RAND_MAX);
		rand_r = laser_segs_max_dev
				* (((float) rand() - RAND_MAX / 2) / RAND_MAX);
		p->vel = cam_r * rand_r * 0.005 + cam_u * rand_u * 0.005;
		p->lifespan = (((float) rand() / (float) RAND_MAX) * 2000) + 500;
		p->time_alive = 0;
		game.particle->add(p);
	}

	// Badass laser
	seg_start = lsr_start;
	if (rand() % 3) {
		glColor3f(0, 0, 1);
	} else {
		glColor3f(1, 1, 0);
	}
	glLineWidth(rand() % laser_max_width);
	glBegin(GL_LINES);
	for (i = 1; i <= laser_segs; i++) {
		seg_end = lsr_start + lsr_seg;
		if (i != laser_segs) {
			rand_u = laser_segs_max_dev
					* (((float) rand() - RAND_MAX / 2) / RAND_MAX);
			rand_r = laser_segs_max_dev
					* (((float) rand() - RAND_MAX / 2) / RAND_MAX);
			seg_end = seg_end + cam_r * rand_r + cam_u * rand_u;
		}
		glVertex3f(seg_start.x, seg_start.y, seg_start.z);
		glVertex3f(seg_end.x, seg_end.y, seg_end.z);
		seg_start = seg_end;
		lsr_seg *= 1.5;

	}
	glEnd();
}

void mouse_callback(int button, int state, int x, int y) {
	if (state == GLUT_DOWN) {
		mouse_states[button] = true;
		if (button == GLUT_LEFT_BUTTON) {
			last_laser_discharge = Utils::get_system_time();
			GameParameters::is_new_shot = true;
		}
	} else {
		mouse_states[button] = false;
	}
}

// motion callback
void motion_callback(int x, int y) {
	glutSetCursor(GLUT_CURSOR_NONE);
	int current_window = glutGetWindow();
	double dx, dy;

	if (current_window == cam_window) {
		int mid_x = glutGet(GLUT_WINDOW_WIDTH ) >> 1;
		int mid_y = glutGet(GLUT_WINDOW_HEIGHT ) >> 1;

		dx = x - mid_x;
		dy = y - mid_y;

		// glutWrapPointer has a bug where it may call the MotionCallback again
		if (dx == 0 && dy == 0) {
			return;
		}
		glutWarpPointer(mid_x, mid_y);

		double x_rad = cam_rot_speed * dx / disp_width;
		double y_rad = cam_rot_speed * dy / disp_height;

		game.player->cam->rotate_view(x_rad, y_rad);
	}
}

void draw_sky() {
	// Draw textured sky hemisphere
	if (!sky_init) {
		glNewList(SKY_LIST, GL_COMPILE_AND_EXECUTE);
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
		glScalef(1000, 1000, 1000);
		glTranslatef(0, -0.25, 0);
		glColor3f(1.0, 1.0, 1.0);
		glBindTexture(GL_TEXTURE_2D, textures[SKY_Q1]);
		draw_quarter_hemisphere(1, sky_stacks);
		glRotatef(90, 0, 1, 0);
		glBindTexture(GL_TEXTURE_2D, textures[SKY_Q2]);
		draw_quarter_hemisphere(1, sky_stacks);
		glRotatef(90, 0, 1, 0);
		glBindTexture(GL_TEXTURE_2D, textures[SKY_Q3]);
		draw_quarter_hemisphere(1, sky_stacks);
		glRotatef(90, 0, 1, 0);
		glBindTexture(GL_TEXTURE_2D, textures[SKY_Q4]);
		draw_quarter_hemisphere(1, sky_stacks);
		glDisable(GL_TEXTURE_2D);
		glEndList();
		sky_init = true;
	} else {
		glCallList(SKY_LIST);
	}

}

void draw_objects(GLenum mode) {
	set<Object*>::iterator itr;
	for (itr = game.phys->objects.begin(); itr != game.phys->objects.end();
			itr++) {
		glPushMatrix();
		Object* obj = *itr;
		if (mode == GL_SELECT) {
			//Names for use in selection mode
			if (obj == game.ai.get_drone()) {
				glLoadName(SEL_AI_ID);
			} else {
				//We only care about the main AI drone
				glLoadName(0);
			}
		}

		glTranslatef(obj->pos.x, obj->pos.y, obj->pos.z);
		obj->draw();
		glPopMatrix();
	}
}

void draw_debug_ground(Object* o) {

	// visually debug ground height and normal
	glPushMatrix();
	glLineWidth(1);
	Vec3 ground_normal = Terrain::get_normal(o->pos.x, o->pos.z);
	Vec3 start = Vec3(o->pos.x, Terrain::get_height(o->pos.x, o->pos.z),
			o->pos.z);
	Vec3 end = start + ground_normal * 10000;
	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
	glVertex3f(start.x, start.y, start.z);
	glVertex3f(end.x, end.y, end.z);
	glEnd();
	glPopMatrix();
}

void draw_axis() {
	glPushMatrix();
	glTranslatef(0, 10, 0);
	glPushMatrix();
	glColor3f(1, 0, 0);
	glTranslatef(1, 0, 0);
	glutSolidCube(1);
	glPopMatrix();
	glPushMatrix();
	glColor3f(0, 1, 0);
	glTranslatef(0, 1, 0);
	glutSolidCube(1);
	glPopMatrix();
	glPushMatrix();
	glColor3f(0, 0, 1);
	glTranslatef(0, 0, 1);
	glutSolidCube(1);
	glPopMatrix();
	glPopMatrix();
}

void draw_grids() {
	glPushMatrix();
	glColor4f(0.5, 0.5, 0.5, 0.5);
	glTranslatef(GameParameters::area_limit, 0, 0);
	glRotatef(90, 0, 0, 1);
	Utils::draw_grid();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-GameParameters::area_limit, 0, 0);
	glRotatef(90, 0, 0, 1);
	Utils::draw_grid();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0, 0, GameParameters::area_limit);
	glRotatef(90, 1, 0, 0);
	Utils::draw_grid();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0, 0, -GameParameters::area_limit);
	glRotatef(90, 1, 0, 0);
	Utils::draw_grid();
	glPopMatrix();
}

void draw_scene() {
	unsigned long dschrg_diff = Utils::get_system_time() - last_laser_discharge;
	if (dschrg_diff < laser_discharge_span)
		draw_laser();
	//draw_debug_ground(game.test_drone);
	glPushMatrix();
	draw_objects(GL_RENDER);
	glPopMatrix();
	glPushMatrix();
	draw_sky();
	glPopMatrix();
	Terrain::draw_terrain();
	// draw_axis();
	game.particle->draw();
}

/*  processHits decides whether the moving robot has been hit.
 *  http://www710.univ-lyon1.fr/~jciehl/Public/OpenGL_PG/ch14.html#id80653
 *
 *  hits = how many objects are potentially selected
 */
void processHits(GLint hits, GLuint *buffer) {
	volatile int i;
	unsigned int j;
	GLuint names;
	GLuint *ptr;

	ptr = buffer;

	// Process every object that may be hit
	for (i = 0; i < hits; i++) {
		//Number of names in the stack for this hit
		names = *ptr;
		ptr += 3; //Skip over min/max depth
		for (j = 0; j < names; j++) {
			/*  for each name */
			if (*ptr == SEL_AI_ID && GameParameters::is_new_shot) {
				GameParameters::is_new_shot = false;
				game.ai.hit();
			}
			ptr++;
			OnScreenDisplay::is_crosshair_ai = true;
		}
	}
}

void pick_ai(int button, int state, int x, int y) {
	OnScreenDisplay::is_crosshair_ai = false;
	GLuint selectBuf[BUFSIZE];
	GLint hits; //number of hits
	GLint viewport[4];

	/*if (button != GLUT_LEFT_BUTTON || state != GLUT_DOWN)
	 return;*/

	glGetIntegerv(GL_VIEWPORT, viewport);

	glSelectBuffer(BUFSIZE, selectBuf);
	glRenderMode(GL_SELECT);

	glInitNames();
	glPushName(-1);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	/*  create 10x10 pixel picking region near cursor location      */
	gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 10.0, 10.0,
			viewport);
	gluPerspective(70.0f,
			float(glutGet(GLUT_WINDOW_WIDTH ))
					/ float(glutGet(GLUT_WINDOW_HEIGHT )), 1.0f, 2000.0f);
	glMatrixMode(GL_MODELVIEW);

	view_from_player();
	draw_objects(GL_SELECT);

	glPopMatrix();
	glFlush();

	hits = glRenderMode(GL_RENDER);
	processHits(hits, selectBuf);
	glutPostRedisplay();
}

/**
 * Calls GLULookAt to make a first-person perspective.
 */
void view_from_player() {
	// Camera is on top of player
	Vec3 pos = game.player->get_eye();
	Camera* cam = game.player->cam;
	Vec3 center = pos + cam->view;
	gluLookAt(pos.x, pos.y, pos.z, center.x, center.y, center.z, cam->up.x,
			cam->up.y, cam->up.z);
}

void draw_3D() {
	glEnable(GL_DEPTH_TEST);

	if (!special_states[GLUT_KEY_F2]) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		is_wireframe = false;
	} else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		is_wireframe = true;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70.0f,
			float(glutGet(GLUT_WINDOW_WIDTH ))
					/ float(glutGet(GLUT_WINDOW_HEIGHT )), 1.0f, 2000.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	view_from_player();

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_COLOR_MATERIAL);
	glLightfv(GL_LIGHT0, GL_POSITION, position0);
	glLightfv(GL_LIGHT1, GL_POSITION, position1);

	draw_scene();
}

// display callback
void display_callback(void) {
	int current_window = glutGetWindow();
	keys_consumer();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw_3D();
	OnScreenDisplay::draw_HUD();

	glutSetWindow(current_window);
	glutSwapBuffers();
}

void draw_title() {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[TITLE]);

	/* create a square on the XY
	 note that OpenGL origin is at the lower left
	 but texture origin is at upper left
	 => it has to be mirrored
	 (gasman knows why i have to mirror X as well) */

	glBegin(GL_QUADS);
	glColor3f(0, 0, 0);

	double rect_length_x;
	double rect_length_y;

	if (is_use_in_values) {
		rect_length_x = in_value[0];
		rect_length_y = in_value[1];
	} else {
		rect_length_x = 1.0;
		rect_length_y = 1.0;
	}

	glNormal3f(0.0, 0.0, 1.0);

	glTexCoord2d(1, 1);
	glVertex3f(-rect_length_x, -rect_length_y, 0.0);
	glTexCoord2d(1, 0);
	glVertex3f(-rect_length_x, rect_length_y, 0.0);
	glTexCoord2d(0, 0);
	glVertex3f(rect_length_x, rect_length_y, 0.0);
	glTexCoord2d(0, 1);
	glVertex3f(rect_length_x, -rect_length_y, 0.0);

	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void render() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* fov, aspect, near, far */
	gluPerspective(60, 1, 1, 10);
	gluLookAt(0, 0, -2, /* eye */
	0, 0, 2, /* center */
	0, 1, 0); /* up */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(0.2, 0.2, 0.2, 0.2);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glutSolidCube(1);
	draw_title();

	glPopAttrib();

	glFlush();

	glutSwapBuffers();
}

void update_state() {
	unsigned long curr_time = Utils::get_system_time();
	int current_window = glutGetWindow();
	if (current_window == cam_window) {
		unsigned long dt = curr_time - prev_phys_time;
		prev_phys_time = curr_time;
		game.phys->advance_state(dt);
		game.particle->step(dt);
		game.ai.update(dt);

		int mid_x = glutGet(GLUT_WINDOW_WIDTH ) >> 1;
		int mid_y = glutGet(GLUT_WINDOW_HEIGHT ) >> 1;
		pick_ai(0, 0, mid_x, mid_y);
	}
}

void idle(int value) {
	if (is_quit) {
		exit(0);
	}

	//glutSetWindow(main_window);
	//glutPostRedisplay();

	if (!paused) {
		update_state();
		glutSetWindow(cam_window);
		glutPostRedisplay();
		glutTimerFunc(TIMER_MS, idle, 0);
	}

}

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
/// Program Entry Point //////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {

	if (argc == 4) {
		is_use_in_values = true;
		for (int x = 1; x < 4; x++) {
			cout << "a" << argv[x];
			in_value[x - 1] = strtod(argv[x], NULL);
		}
	} else {
		for (int x = 0; x < 3; x++) {
			in_value[x] = 0;
		}
	}

	if (argc == 2) {
		if (strcmp(argv[1], "test") == 0) {
			Terrain::res = 5;
			Terrain::map = Terrain::MAP_BLAND;
			GameParameters::area_limit = 50;
		}
	}

// initialize glut
	glutInit(&argc, argv);

// use double-buffered RGB+Alpha framebuffers with a depth buffer.
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

// initialize the mothership window
	/*glutInitWindowSize(disp_width, disp_height);
	 glutInitWindowPosition(0, 100);
	 main_window = glutCreateWindow("AURUA");
	 glutKeyboardFunc(keyboard_callback);
	 glutDisplayFunc(render);
	 glutReshapeFunc(resize_callback);
	 glutSetWindow(main_window);
	 init();*/

// initialize the camera window
	glutInitWindowSize(disp_width, disp_height);
	glutInitWindowPosition(disp_width + 50, 100);
	cam_window = glutCreateWindow("AURUA");
	glutKeyboardFunc(cam_keyboard_callback);
	glutKeyboardUpFunc(cam_keyup_callback);
	glutSpecialFunc(special_key_callback);
	glutDisplayFunc(display_callback);
	glutReshapeFunc(resize_callback);
	glutMouseFunc(mouse_callback);
	glutPassiveMotionFunc(motion_callback);
	glutMotionFunc(motion_callback);
	glutSetWindow(cam_window);
	gl_init();
	game_init();

	// load image and create textures
	Textures::create_textures();

	glutTimerFunc(TIMER_MS, idle, 0);
	//glutIdleFunc(idle);

// start the main loop
	glutMainLoop();

	return 0;
}
