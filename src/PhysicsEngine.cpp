/*
 * PhysicsEngine.cpp
 *
 *  Created on: Mar 30, 2013
 *      Author: Edward
 */

#include <vector>
#include <stdio.h>

#include "PhysicsEngine.h"
#include "Terrain.h"
#include "GameParameters.h"

PhysicsEngine::PhysicsEngine(Vec3 min, Vec3 max) {
	Bound bound = Bound(min, max);
	octree = new Octree(bound, false);
}

PhysicsEngine::~PhysicsEngine() {
	delete octree;
}

void PhysicsEngine::add_object(Object* o) {
	this->objects.insert(o);
}

void PhysicsEngine::advance_state(float t) {
	pre_move_change(t);
	update_objects_position(t);
	handle_collisions();
	post_move_change(t);
}

void PhysicsEngine::pre_move_change(float dt_ms) {
	float dt_sec = dt_ms / 1000.0;
	for (set<Object*>::iterator itr = objects.begin(); itr != objects.end();
			itr++) {
		Object* obj = *itr;
		if (obj->mass > 0) {
			obj->acc += Vec3(0, GRAVITY, 0);
		}
		bool on_ground = object_on_ground(obj);
		Vec3 ground_normal = Terrain::get_normal(obj->pos.x, obj->pos.z);
		// slows object running up hill
		if (on_ground && obj->vel.length() != 0
				&& ground_normal.length() != 0) {
			double lens_product = ground_normal.length() * obj->vel.length();
			//debug_vec3(ground_normal);
			//debug_vec3(obj->vel);

			double cos_angle = ground_normal.dot(obj->vel) / lens_product;
			double mod = cos_angle;
			if (mod > 0)
				mod = 0;
			obj->acc += obj->vel * mod + ground_normal;
			//printf("cos_angle %f\n", cos_angle);
			//printf("mod %f\n\n", mod);
		}
	}
}

void PhysicsEngine::update_objects_position(float dt_ms) {
	float dt_sec = dt_ms / 1000.0;
	for (set<Object*>::iterator itr = objects.begin(); itr != objects.end();
			itr++) {
		Object* obj = *itr;

		// Accelerate
		if (obj->acc.length() != 0) {
			obj->vel += obj->acc * dt_sec;
			obj->acc = Vec3(0, 0, 0);
		}

		// Move object
		Vec3 new_pos = obj->pos + obj->vel * dt_sec;
		Terrain::bounds_check(new_pos);
		obj->pos = new_pos;
	}
}

void PhysicsEngine::handle_collisions() {
	unsigned int i;
	vector<ObjectPair> pairs;
	octree->get_object_pairs(pairs);
	for (i = 0; i < pairs.size(); i++) {
		ObjectPair pair = pairs[i];
		Object* obj_1 = pair.obj_1;
		Object* obj_2 = pair.obj_2;
		if (obj_1->proxy && obj_2->proxy
				&& obj_1->proxy->collide(obj_2->proxy)) {
			rebounce_objects(obj_1, obj_2);
		}
	}
}

bool PhysicsEngine::object_on_ground(Object* obj) {
	return abs(obj->pos.y - Terrain::get_height(obj->pos.x, obj->pos.z))
			< ABS_GROUND_TOL;
}

void PhysicsEngine::post_move_change(float dt_ms) {
	float dt_sec = dt_ms / 1000.0;
	for (set<Object*>::iterator itr = objects.begin(); itr != objects.end();
			itr++) {
		Object* obj = *itr;
		if (obj->mass > 0) {
			double ground_y = Terrain::get_height(obj->pos.x, obj->pos.z);
			bool on_ground = object_on_ground(obj);

			// Prevent objects going under ground
			if (obj->pos.y < ground_y) {
				obj->pos.y = ground_y;
			}
			// Prevent object going out of bounds
			if (obj->pos.x < -GameParameters::area_limit) {
				obj->pos.x = -GameParameters::area_limit;
			} else if (obj->pos.x > GameParameters::area_limit) {
				obj->pos.x = GameParameters::area_limit;
			}
			if (obj->pos.z < -GameParameters::area_limit) {
				obj->pos.z = -GameParameters::area_limit;
			} else if (obj->pos.z > GameParameters::area_limit) {
				obj->pos.z = GameParameters::area_limit;
			}

			if (on_ground) {
				// Apply unrealistic friction
				double new_kin = pow(1 - FRICTION, dt_sec);
				obj->vel.x *= new_kin;
				obj->vel.z *= new_kin;
				obj->vel.y = 0;

			}

		}
		octree->remove(obj);
		octree->add(obj);
	}
}

void PhysicsEngine::reflect_objects(Object* obj_1, Object* obj_2) {
	// Reflect object velocities around normal of collision
	// disregard momentum, no energy loss
	// vel' = vel - 2*(n*(n dot vel))

	Vec3 n = obj_1->pos - obj_2->pos;
	float net_mass = obj_1->mass + obj_2->mass;
	n.normalize();
	obj_1->vel -= n * (n.dot(obj_1->vel)) * 2 * (obj_2->mass / net_mass);
	obj_2->vel -= n * (n.dot(obj_2->vel)) * 2 * (obj_1->mass / net_mass);
}

void PhysicsEngine::rebounce_objects(Object* obj_1, Object* obj_2) {
	float net_mass = obj_1->mass + obj_2->mass;
	float net_magnitude = obj_2->vel.length() + obj_1->vel.length();
	// separate immobile things
	if (net_magnitude == 0) {
		net_magnitude = NORMAL_FORCE;
	}
	Vec3 disp_1 = obj_1->pos - obj_2->pos;
	disp_1.normalize();
	Vec3 disp_2 = obj_2->pos - obj_1->pos;
	disp_2.normalize();

	obj_1->vel = disp_1 * (net_magnitude * (obj_2->mass / net_mass));
	obj_2->vel = disp_2 * (net_magnitude * (obj_1->mass / net_mass));
}

void PhysicsEngine::add(Object* obj) {
	objects.insert(obj);
	octree->add(obj);
}

void PhysicsEngine::remove(Object* obj) {
	objects.erase(obj);
	octree->remove(obj);
}

void PhysicsEngine::bounds_check(Vec3& vec) {
	if (abs(vec.x) >= GameParameters::area_limit) {
		vec.x = (vec.x > 0) ?
				GameParameters::area_limit : -GameParameters::area_limit;
	}

	if (abs(vec.z) >= GameParameters::area_limit) {
		vec.z = (vec.z > 0) ?
				GameParameters::area_limit : -GameParameters::area_limit;
	}
}
