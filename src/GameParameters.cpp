/*
 * GameParameters.cpp
 *
 *  Created on: Apr 3, 2013
 *      Author: Billy
 */

#include "GameParameters.h"

namespace GameParameters
{
	//The playing area ranges from -area_limit to area_limit
	//in both X and Z
	float area_limit = DEFAULT_AREA;

	bool is_show_fps = false; //Show FPS counter?
	bool is_new_shot = false; //Did the player shoot again?

	bool is_game_over = false; //Is game over?
	bool is_ai_win = false; //Did the AI win?
}
