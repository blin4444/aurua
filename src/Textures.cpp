#include "Textures.h"
#include <stdio.h>
#include <stdlib.h>

namespace Textures {
GLuint textures[NUM_TEXTURES];
/**
 * From http://en.wikibooks.org/wiki/OpenGL_Programming/Intermediate/Textures
 */
GLuint raw_texture_load(GLuint textures[], const char *filename, int width,
		int height, int texture_id, bool is_modulate) {
	unsigned char *data;
	FILE *file;
	GLfloat border_color[] = { 0, 0, 0, 0 };

	// open texture data
	file = fopen(filename, "rb");
	if (file == NULL) {
		fprintf(stderr, "Error: cannot open %s.\n", filename);
		fflush(stderr);
		return 0;
	}

		// allocate buffer
	data = (unsigned char*) malloc(width * height * 4);

	// read texture data
	fread(data, width * height * 4, 1, file);
	fclose(file);

	// allocate a texture name
	glGenTextures(1, &textures[texture_id]);

	// select our current texture
	glBindTexture(GL_TEXTURE_2D, textures[texture_id]);

	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,
			(is_modulate) ? GL_MODULATE : GL_DECAL);

	// build our texture mipmaps
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE,
			data);

	// free buffer
	free(data);

	return 1;
}

GLuint raw_alpha_load(GLuint textures[], const char *filename, int width,
		int height, int texture_id) {
	unsigned char *data, *alpha;
	int y, x;
	FILE *file;
	GLfloat border_color[] = { 0, 0, 0, 0 };

	// open texture data
	file = fopen(filename, "rb");
	if (file == NULL) {
		fprintf(stderr, "Error: cannot open %s.\n", filename);
		fflush(stderr);
		return 0;
	}

		// allocate buffer
	data = (unsigned char*) malloc(width * height * 4);
	alpha = (unsigned char*) malloc(width * height * 4);

	// read texture data
	fread(data, width * height * 4, 1, file);
	fclose(file);

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			for (int j = 0; j < 3; j++) {
				alpha[4 * (y * width + x) + j] = 255;
			}
			alpha[4 * (y * width + x) + 3] = data[3 * (y * width + x) + 3];
		}
	}

	// allocate a texture name
	glGenTextures(1, &textures[texture_id]);

	// select our current texture
	glBindTexture(GL_TEXTURE_2D, textures[texture_id]);
	// glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, alpha);

	// free buffer
	delete[] data;
	delete[] alpha;

	return 1;
}

void create_textures() {
	raw_texture_load(textures, "textures/aurua.raw", 1772, 1772, TITLE, false);
	raw_texture_load(textures, "textures/sky_q1.raw", 512, 512, SKY_Q1, false);
	raw_texture_load(textures, "textures/sky_q2.raw", 512, 512, SKY_Q2, false);
	raw_texture_load(textures, "textures/sky_q3.raw", 512, 512, SKY_Q3, false);
	raw_texture_load(textures, "textures/sky_q4.raw", 512, 512, SKY_Q4, false);
	raw_texture_load(textures, "textures/sand.raw", 512, 512, SAND, true);
	raw_alpha_load(textures, "textures/circlealpha.raw", 256, 256, PARTICLE);
}
}
