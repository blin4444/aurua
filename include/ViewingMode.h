#pragma once

enum ViewingMode {
	LOOKAT,
	FLYING,
	ORBIT
};
