/*
 * OnScreenDisplay.h
 *
 *      Author: Billy
 */
#ifndef ONSCREENDISPLAY_H_
#define ONSCREENDISPLAY_H_
#include <ctime>

namespace OnScreenDisplay
{
	// The time at which the last message was displayed.
	extern time_t last_message_time;
	extern const char* last_message;

	//Is the crosshair currently pointing at the AI drone?
	extern bool is_crosshair_ai;
	extern char last_fps[8];

	void print_life();
	void draw_HUD();

	void draw_crosshair(bool is_on, float h_w_ratio);

	void print_bitmap_string(void* font, char* s);
	void print_stroke_string(void* font, char* s);

	void set_fps(char* buf);

} /* namespace NixonEval */
#endif /* ONSCREENDISPLAY_H_ */
