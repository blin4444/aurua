/*
 * GameConstants.h
 *
 *  Created on: Apr 3, 2013
 *      Author: Billy
 */

#ifndef GAMECONSTANTS_H_
#define GAMECONSTANTS_H_

namespace GameParameters
{
	const float DEFAULT_AREA = 200;
	extern float area_limit;

	extern bool is_game_over;
	extern bool is_ai_win;
	extern bool is_show_fps;

	extern bool is_new_shot;

	/**
	 * The distance between the AI and the
	 * player that results in death
	 */
	const double AI_DEATH_TOLERANCE = 4;
	const double AI_MSG_TOLERANCE = 40;
}

#endif /* GAMECONSTANTS_H_ */
