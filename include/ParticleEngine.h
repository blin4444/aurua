/*
 * ParticleEngine.h
 *
 *  Created on: Apr 7, 2013
 *      Author: Edward
 */

#ifndef PARTICLEENGINE_H_
#define PARTICLEENGINE_H_
#include <vector>
#include "Vec3.h"
#include "Textures.h"
#include "Player.h"

using std::vector;

typedef struct Particle_t {
	Vec3 pos;
	Vec3 vel;
	Vec3 color;
	float time_alive;
	float lifespan;

} Particle;
const float PARTICLE_SIZE = 0.5f;
class ParticleEngine {
public:
	ParticleEngine(Player* _player);
	virtual ~ParticleEngine();
	vector<Particle*> particles;
	bool operator()(Particle* p1, Particle* p2);
	void add(Particle* p);

	//Advances every particle by dt_ms millisecond
	void step(float dt_ms);
	void draw();

	// returns random float between -1 and 1
	float rand_float();

	Vec3 rand_vec3();
	Player* player;
};

#endif /* PARTICLEENGINE_H_ */
