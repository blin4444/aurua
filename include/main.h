#ifndef MAIN_H_
#define MAIN_H_

#include "Game.h"

extern bool sky_init;

//The number of milliseconds to which the timer is set
const int TIMER_MS = 16;

// Selection ID for AI drone
const unsigned int SEL_AI_ID = 1;

// select buffer size
#define BUFSIZE 512

// flag to indicate that we should clean up and exit
extern bool is_quit;
extern bool paused;

extern bool is_crosshair_ai;

// Keys variables
extern bool key_states[256];
extern bool special_states[256];
extern bool mouse_states[3];

// window handles
extern int main_window, cam_window;
extern unsigned long prev_fps_time;
extern unsigned long prev_phys_time;
extern int frame_passed;
extern char win_title[32];

void player_jump();
void view_from_player();

// display width and height
extern int disp_width;
extern int disp_height;

// Cursor previous position
extern int prev_x, prev_y;

//Cool debug tool! Use command line arguments to pass in ints!
extern bool is_use_in_values;
extern double in_value[3];
extern double cam_rot_speed;

void idle(int);
void motion_callback(int x, int y);

void pick_ai(int button, int state, int x, int y);

void init_pick();

extern Game game;

const int laser_segs = 50;
const float laser_segs_max_dev = 0.75f;
const int laser_max_width = 10;
extern unsigned long last_laser_discharge;
const unsigned long laser_discharge_span = 500;

#endif
