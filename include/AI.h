/*
 * AI.h
 *
 *  Created on: Mar 31, 2013
 *      Author: Billy
 */
#ifndef AI_H_
#define AI_H_
#include "Vec3.h"
#include "Object.h"
#include "Drone.h"
#include <time.h>
#include <string>

enum AIState
{
	AI_SELECT,
	AI_GOTO,
	AI_STANDBY,
	AI_EVADE,
	AI_KILL
};

class AI
{
public:
	AI();
	virtual ~AI();

	void update(float dt);

	void render();

	void set_representative(Drone* drone);
	void set_enemy(Object* enemy);

	void go_to_random_pos();
	void evade_enemy();
	virtual void kill_enemy();

	Vec3& get_pos();

	Drone* get_drone();

	void hit();

	int getHealth() const
	{
		return health;
	}

	bool has_message();

	/**
	 * AI close to player!
	 */
	bool is_close(bool is_very);

	const char* get_message();

private:
	AIState state;
	Object* enemy;
	Drone* drone;

	//The random position to go to when bored.
	Vec3 go_to_pos;
	Vec3 start_pos;

	Vec3 get_enemy_pos();
	Vec3 select_pos();
	void standby();

	unsigned int step;
	unsigned int totalSteps;

	void make_decision();

	// How much to travel per step
	float diffX;
	float diffZ;

	unsigned int standby_steps;

	clock_t last_time;
	clock_t last_render_hit_time;

	int health;

	bool is_messaging;
};

#endif /* AI_H_ */
