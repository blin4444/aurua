/*
 * Proxy.h
 *
 *  Created on: 2013-04-05
 *      Author: Edward
 */

#ifndef PROXY_H_
#define PROXY_H_
#define NOT_A_TYPE -1
#define SPHERE_PROXY 0
#define AABB_PROXY 1

#include "Object.h"

class Proxy {
public:
	Proxy();
	virtual ~Proxy();
	virtual bool collide(Proxy*);
	virtual int get_type();
	Object* object;
};

#endif /* PROXY_H_ */
