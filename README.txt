AURUA

High Level Explanation
We created a first-person shooter video game, where you chase and shoot a robot.
-Advanced features
	Space partitioning: Octree for preprocessing collision testing
	Particle system: Fancy laser beams
	Terrain generation: Procedural terrain generation and terrain following
	
Mid-Level Description:

Collision Testing: collision tests are done on the spherical proxies of in-game objects, There is an octree that the game uses for preliminarily reducing potential collision pairs. Collisions are determined by computing the distance between two spheres and their velocities at the moment; two objects are colliding if their centers have distance less than the sum of their proxies' radii and their velocity difference is converging with their position difference. The octree separates space into eight subregions recursively until it hits a static lower bound. The leaves contain all the collidable objects in game, the objects inside each leaf are returned as potential collisions for each frame and the pairs are then tested for actual collision. The octree implementation uses lazy initialization and subtrees that not initialized are not checked and do not return any object pairs. The lazy initialization property is maintained dynamically as objects move around in game, subtree that become non-empty are initialized and those that become empty are uninitialized. When two objects collided they are rebounded off each other, the velocities after the rebounce depends on the objects' masses and their velocity before the collision. 

Particle system: Smoke from laser, the smoke particle is a 2D circle that always faces the camera and deviates from the laser's line in a random direction and slowly fades away. The 2D circle has its alpha textured so that it is opaque in the center and turns transparent gradually we radiates out. The circle is built with type GL_QUADS its vertices are computed using orthogonal bases of the view coordinates, hence always facing the camera. Each particle is created with a lifespan and an alive value, the alive value gets increaed every frame and when it reaches the lifespan of the lifespan of the particle the particle will vanish and be removed from the game. The transparency of each particle at each frame is computed using the alive value.

Terrain generation: The generation of terrain is separated from the rendering. An array of random numbers are generated, with larger numbers at the edges and corners. This array represents the heights in a terrain grid. In addition to this array, an array of normals for each vertex, and an array of indices to use with GL_TRIANGLE_STRIP, are generated. The function Terrain::get_height provides the height y of any x, z position on a triangle strip for terrain following. Another map using trigonometric functions as well as rand() is available.

Artificial Intelligence:
The robot is modeled using a finite-state machine. The robot picks a random point on the map and incrementally goes to it. It sometimes decides to pause for a bit. Otherwise, it will repeat with a new point on the map. The decisions are based on the rand() function. When the robot is near the user, it picks a random insult and the insult is displayed on the screen.

Picking:
A projection matrix that defines a small volume seen around the cursor is used with the GL_SELECT mode. OpenGL renders the objects in this small volume and returns a hit record of every object in that volume. The hit record contains the name stack during time of rendering, which we use to tell if the AI drone should be hit with the laser.

Controls
- W,A,S,D to move player
- Move mouse to pan view
- Click to shoot at point indicated by crosshair
- Must click repeatedly, cannot hold click

Objective
- Kill the moving robot before it runs you over!

SOURCES:

General:

- First-person-shooter style video games for the basic idea

- OpenGL Programming Guide
	- Selection / Picking
	- Textures
	http://www710.univ-lyon1.fr/~jciehl/Public/OpenGL_PG/
	
Loading textures:
	http://en.wikibooks.org/wiki/OpenGL_Programming/Intermediate/Textures

Particles and collision:
	http://www.videotutorialsrock.com/

FPS calculation and camera:
	http://www.morrowland.com/apron/tut_gl.php

Sky dome:
	http://stackoverflow.com/questions/7687148/drawing-sphere-in-opengl-without-using-glusphere